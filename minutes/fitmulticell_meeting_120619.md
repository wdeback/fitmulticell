# FitMultiCell meeting 12.06.2019

## Participants

- Jörn Starruß
- Robert Müller
- Walter de Back

## deRSE

Jörn was present at the 1st deRSE meeting in Postdam (https://www.de-rse.org/en/conf2019/).
This included a meeting with other awardees of the DFG Grant on Sustainable Software Development. 

- Ideas were exchanged on obtaining long-term financing of software projects, including a model where financing depends directly on usage and citation statistics.

- Contact was established with Ilastik developer (Dominik Kutna) from EMBL. Discussed possibility of Ilastik <-> Morpheus demo. Send segmentated images directly from Ilastik to Morpheus, even potentially using time-series of segmentation images for simulationmodel fitting (link to FitMultiCell).


## Jörn: Continuous integration

- Currently Jörn has implemented whitebox tests for scoping, vector calculations and system solver tests. These should suffice to enable Robert to make use of these tests for his implementation of implicit solvers for PDEs (see below).

- Tests are implemented using `CTest` framework.

- A framework for integration tests will be implemented next. These will probably consist of XML + text file with expected values. First tests: 
  - stochastic cell motility model: multiple repititions, collect statistics, tolerance
  - stochastic cell division model (e.g. exponential growth): compare with theorectical prediction

- Walter mentions continuous integration and continuous deployment of a C++/Qt project set up by Sebastian Wagner at IMB. Provides automated multi-platform building of TraCurate (https://tracurate.gitlab.io/). Planning to set up dedicated server to build (runners) on Mac OSX.

## Robert: implicit PDE solvers

- Robert will implement adaptive implicit solvers for PDE for diffusion and advection. This will allow for heterogenous diffusion and advection and provide faster and more robust computation. Implementation will be based on `Eigen` library (already included in Morpheus).

- He will structure this to enable intracelluar PDEs (without moving boundaries), similar to Stan Maree, see https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3291540/.

## CPM performance

- new branch opened with CPM optimized for memory footprint. 
  - edge tracker was optimized by adding an stack of empty lists (saving memory allocation) and garbage collection.
  - this enabled simulation of ~400,000 cells using 150Gb RAM.
  - still room for improvement
  
- We discussed options for CPM parallelization:
  - openMPI: checkerboard domain composition as in CompuCell3D (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2139985/)
    - pros: scalable to large CPU clusters, not bound to shared memory nodes. 
    - cons: load-balancing issues, e.g. when many cells present in small region of space.
  - openMP: 
    - parallelize independent CPM updates from edge tracker list
    - lock cell + neighboring cells
    - evaluate updated in parallel, but execute sequentially (because edge tracker is not thread-safe)
    - pros: relatively straight-forward solution, less issues with load balancing
    - cons: not scalable beyond shared memory nodes.

## Morpheus release 2.1

- code is ready for 2.1
- major improvements:
  - much improved SBML compatibility (compartments, events, delay), SBML test suite passed.
  - implemented adaptive (RK4) solvers for ODEs
- to do (Walter):
  - advertisement: blog posts, letter to users/colleagues/supporters, twitter campaign. 

- plans for 2.2:  
  - improvements in usability  
  - migration to Qt 5: new browser system
  - interactive symbol / computational graph
  - implicit solvers for PDEs?

## PyABC interface

- Walter will write an initial simple pyABC <-> Morpheus integration as baseline:
   - start Morpheus with system call
   - parameters are transfered using command line interface `-key=value` construct
   - Morpheus writes  CSV file written by `Logger`
   - get summary statistics as `pandas DataFrame` or `dict`

- Next steps:
   - transfer paraeters via XML (e.g. `etree`)
   - multithreading
   - Python bindings:
     - extend current `pybind11` interface from `Analysis` (output only) to `Reporters` (output and feedback into simulation) 
     - start/stop Morpheus directly via python bindings?
     - make XML internal structure accessible via python bindings?
  

