import os
from threading import Thread
from concurrent.futures import Future

def call_with_future(fn, future, args, kwargs):
    try:
        result = fn(*args, **kwargs)
        future.set_result(result)
    except Exception as exc:
        future.set_exception(exc)

def threaded(fn):
    def wrapper(*args, **kwargs):
        future = Future()
        Thread(target=call_with_future, args=(fn, future, args, kwargs)).start()
        return future
    return wrapper


class Morpheus(object):
    '''
    Thin wrapper to run a Morpheus simulation from python.
    
    It provides the simplest possible pyABC <-> Morpheus integrations:
    - it runs morpheus as subprocess.check_call.
    - it uses morpheus' command line arguments to override parameters values specified in XML.
    
    '''
    
    def __init__(self, 
                 morpheus_exec_path:str, 
                 model_path:str, 
                 results_fn:str, 
                 verbose:bool=False, 
                 output:str='dict') -> None:
        '''
            Args:
                morpheus_exec_path (str): absolute path to morpheus simulator executable
                model_path (str): absolute path to morpheus (xml) model
                results_fn (str): filename of results file 
        '''
        # simulator
        if not os.path.exists(morpheus_exec_path):
            raise ValueError(f'Simulator executable does not exist: {morpheus_exec_path}')
        else:
            self.simulator = morpheus_exec_path
        
        # model
        if not os.path.exists(model_path):
            raise ValueError(f'Model file does not exist: {model_path}')
        else:
            self.model = model_path
            
        # path of results
        self.results_fn = results_fn

        # output format
        self.output = output
        
        # verbosity
        self.verbose = verbose
        
        # command
        self.command = None
        # results
        self.result_df = None
        
    def __str__(self):
        s = f'simulator:\t{self.simulator}\n'
        s += f'model:\t{self.model}\n'
        s += f'results:\t{self.results_fn}\n'
        s += f'output:\t{self.output}\n'
        s += f'verbosity:\t{self.verbose}\n'
        return s
    
    def __repr__(self):
        return self.__str__()
    
    def set_verbosity(self, 
                      value:bool) -> None:
        self.verbose = value

    def set_output(self, 
                   format:str) -> None:
        '''
        Set output format to dataframe or dict.
        
        Args
        '''
        formats = ['dataframe', 'dict']
        if format in formats:
            self.output = format

    def simulate(self, params_dict:dict):
        future = self._simulate(params_dict)
        result = future.result() # will block until thread returns
        return result
    
    ## TODO: make this THREAD-SAFE
    ## currently, two subsequent calls can result in a command that appends two strings, resulting in an invalid [key,value] command line arguments 
    @threaded
    def _simulate(self, params_dict:dict):
        
        # parameters
        if len(params_dict) == 0:
            raise ValueError(f'No parameters specified: {params_dict}')
        params_dict = dict(params_dict)
        if not isinstance(params_dict, dict):
            raise ValueError(f'Parameters not specified as a dict: {params_dict}')

        from tempfile import TemporaryDirectory

        # execute in temporary directory, python 3.5+
        with TemporaryDirectory(prefix='morpheus_pyabc_') as tmpdir:

            # change into tmp directory
            os.chdir( tmpdir )

            # generate command
            #  -file: XML model
            #  -[key]=[value]: parameters
            self.command = '{morpheus} -file {model}'.format(morpheus=self.simulator, model=self.model)
            for k,v in params_dict.items():
                self.command += ' -{}={} '.format(k,v)

            if self.verbose:
                print(self.command)
                print()
                
            # run morpheus as subprocess
            from subprocess import CalledProcessError, check_call, STDOUT
            try:
                FNULL = open(os.devnull, 'w')  # redirect stdout and stderr to DEVNULL
                check_call(self.command, shell=True, stdout=FNULL, stderr=STDOUT, close_fds=True)
            except CalledProcessError as e:
                raise RuntimeError(f'SIMULATION ERROR: {e.returncode} (err: {e.output})')
                return None

            # read results from file
            self.get_results(tmpdir)

            if self.verbose:
                print(self.result_df)
                print()

            if self.output == 'dataframe':
                return self.result_df
            elif self.output == 'dict':
                # convert dataframe to dictionary where each value contains column data as a list
                return self.result_df.to_dict(orient='list')
            
        return None
        
#     def run(self):
#         from subprocess import CalledProcessError, check_call, STDOUT
#         try:
#             FNULL = open(os.devnull, 'w')  # redirect stdout and stderr to DEVNULL
#             check_call(self.command, shell=True, stdout=FNULL, stderr=STDOUT, close_fds=True)
#         except CalledProcessError as e:
#             raise RuntimeError(f'SIMULATION ERROR: {e.returncode} (err: {e.output})')
#             return None

        
    def get_results(self, tmpdir):
        import pandas as pd
        # read results from file
        results_path = os.path.join(tmpdir, self.results_fn)
        if os.path.exists( results_path ):
            self.result_df = pd.read_csv( results_path , sep='\t')
        return self.result_df

        